#!/usr/bin/env python

#Author : Karim Baïna, ENSIAS, Mohammed V University
#Research Team : Alqualsadi research team (Innovation on Digital and Enterprise Architectures)
#                ADMIR Laboratory, Rabat IT Center,
#Institution : ENSIAS, University Mohammed V in Rabat,
#              BP 713 Agdal, Rabat, Morocco
#e-mail : karim.baina@um5.ac.ma, karim.baina@gmail.com
#twitter : @kbaina
#Date : April 2nd, 2020; June 5th, 2020
#http://ensias.um5.ac.ma/professor/m-karim-baina
#https://scholar.google.com/citations?user=tRtXdkEAAAAJ&hl=fr
#http://www.slideshare.net/kbaina
#https://gitlab.com/kbaina

import sys
import re
from datetime import datetime
import csv  
import math 

master_data_home = 'master_data'
integation_data_home = 'output_data'
DEMOGRAPHY_MASTER_DATA = "."+"/"+master_data_home+"/"+"demography.csv"
#MEDIAN_AGE_MASTER_DATA = "."+"/"+home+"/"+"median_age.csv"
COUNTRIES_BY_CONTINENTS_MASTER_DATA = "."+"/"+master_data_home+"/"+"countries_by_continents.csv"
ARAB_COUNTRY_MASTER_DATA = "."+"/"+master_data_home+"/"+"arab_countries.csv"
ICESCO_COUNTRY_MASTER_DATA = "."+"/"+master_data_home+"/"+"icesco_countries.csv"
MONITORED_COUNTRY_MASTER_DATA = "."+"/"+master_data_home+"/"+"monitored_countries.csv"
SCHENGEN_COUNTRY_MASTER_DATA = "."+"/"+master_data_home+"/"+"schengen_countries.csv"
EUROZONE_COUNTRY_MASTER_DATA = "."+"/"+master_data_home+"/"+"euro_zone_countries.csv"
G7_COUNTRY_MASTER_DATA = "."+"/"+master_data_home+"/"+"G7_countries.csv"    
specific = sys.argv[1]
JHU_DATA_FORMATTED_DATA_SET = "."+"/"+integation_data_home+"/"+"time_series_covid19_"+specific+"_global-us1-normalisation.csv"

Province_State = 0
Country_Region = 1
Latitude = 2
Longitude = 3

#median_age=2

pop = {}
density = {}
#landarea = {}
mediumage = {}
#medianage = {}
urban_population_rate = {}
continent = {}
country_days_since_day0 = {}
country_day_zero_case_index = {}
distance = {}
fileList = []
country_row = {}
#cluster = {}
#country_distance = {}
arab_country = {}
icesco_country = {}
monitored_country = {}
monitored_countries = []
schengen_country = {}
g7_country = {}
eurozone_country = {}

def compute_distance(country1_row, country2_row):
  
  country1 = country1_row[Country_Region].lower()
  country2 = country2_row[Country_Region].lower()
  distance[(country1 , country2 )] = 0.0
  country_1_detection_age = country_days_since_day0 [ country1 ]
  country_2_detection_age = country_days_since_day0 [ country2 ]

  country_detection_min_age = min(country_1_detection_age, country_2_detection_age)
  
  i1 = country_day_zero_case_index[ country1 ]
  i2 = country_day_zero_case_index[ country2 ]

  for i in range(country_detection_min_age):
     #sqrt(sum ( (f(x)-g(x))**2))
     distance[ ( country1, country2 ) ] += pow(int(country1_row[ i1 ]) - int(country2_row[ i2 ]), 2)
     i1 += 1
     i2 += 1

  # pour des besoins d'indexation bi-dimensionnelle
  distance[ ( country1, country2 ) ] = math.sqrt ( distance[ ( country1, country2 ) ] )
  distance[ ( country2, country1 ) ] = distance[ ( country1, country2 ) ] # matrix symétrique

  # pour des besoins de tri par 
  #country_distance[ country1 ] = [country2, distance[ ( country1, country2 ) ]]
  #country_distance[ country2 ] = [country1, distance[ ( country1, country2 ) ]]

def closeOpenFiles():
  for f in fileList:
    f.close()

def main():
  D18932020_key = 60
  D18932020_value = 54
  column_labels_Flag = True
  
  #for country in monitored_countries:
  #  for sphere in range(1,5): #{2, 4}
  #     cluster[ ( country, sphere ) ]=[]
  
  
  # source : https://www.worldometers.info/world-population/population-by-country/
  # schema : ID,Country,Population_2020,Yearly_Change,Net_Change,Density_P_Km2,LandArea_Km2,Migrants_Net,Fetility_Rate,Medium_Age,Urban_Population,World_Share
  ID=0
  Country=1
  Population_2020=2
  Yearly_Change=3
  Net_Change=4
  Density_P_Km2=5
  LandArea_Km2=6
  Migrants_Net=7
  Fetility_Rate=8
  Medium_Age=9
  Urban_Population=10
  World_Share=11
  
  demographyfile = open(DEMOGRAPHY_MASTER_DATA, mode = 'r', newline='')
  demographyreader = csv.reader(demographyfile,delimiter=',') 
  fileList.append( demographyfile )
  
  for poprow in demographyreader:
      #print(poprow)
      if column_labels_Flag == True:
          column_labels_Flag = False
      else:
          pop[ (poprow[Country]).lower() ] = poprow[ Population_2020 ]
          density[ (poprow[Country]).lower() ] = poprow[ Density_P_Km2 ]
          mediumage[ (poprow[Country]).lower() ] = poprow[ Medium_Age ]
          urban_population_rate[ (poprow[Country]).lower() ] = poprow[ Urban_Population ]
          #landarea[ (poprow[Country]).lower() ] = poprow[ LandArea_Km2 ]
          #print("pop[ ", (poprow[Country]).lower()," ] = ", pop[ (poprow[Country]).lower() ] )
  
  # source : https://www.cia.gov/library/publications/the-world-factbook/fields/343rank.html
  #medianagefile = open(MEDIAN_AGE_MASTER_DATA, mode = 'r', newline='')
  #medianagereader = csv.reader(medianagefile,delimiter=',') 
  #fileList.append( medianagefile )
  #
  #for medianagerow in medianagereader:
      #print(medianagerow)
  #    if column_labels_Flag == True:
  #        column_labels_Flag = False
  #    else:
  #        medianage[ (medianagerow[Country]).lower() ] = medianagerow[ median_age ]
  
  # source : wikipedia https://en.wikipedia.org/wiki/Arab_League
  arabcountryfile = open(ARAB_COUNTRY_MASTER_DATA, mode = 'r', newline='')
  arabcountryreader = csv.reader(arabcountryfile,delimiter=',') 
  fileList.append( arabcountryfile )
  for arabcountryrow in arabcountryreader:
      #print(medianagerow)
      arab_country[ arabcountryrow[0].lower() ] = True
  
  # source : wikipedia https://en.wikipedia.org/wiki/Islamic_Educational,_Scientific_and_Cultural_Organization
  icescocountryfile = open(ICESCO_COUNTRY_MASTER_DATA, mode = 'r', newline='')
  icescocountryreader = csv.reader(icescocountryfile,delimiter=',') 
  fileList.append( icescocountryfile )
  for icescocountryrow in icescocountryreader:
      #print(medianagerow)
      icesco_country[ icescocountryrow[0].lower() ] = True
  
  # source : https://unstats.un.org/unsd/methodology/m49/overview/
  # schema : ID, Country, Iso-Alpha3-Code, M49-code, Region1, Region2, Continent
  column_labels_Flag = True
  Continent_index = 6
  
  continentsfile = open(COUNTRIES_BY_CONTINENTS_MASTER_DATA, mode = 'r', newline='')
  continentsreader = csv.reader(continentsfile,delimiter=',') 
  fileList.append( continentsfile )
  
  for continentrow in continentsreader:
      if column_labels_Flag == True:
          column_labels_Flag = False
      else:
          continent[ (continentrow[Country]).lower() ] = continentrow[ Continent_index ]
   
  #source : https://en.wikipedia.org/wiki/Schengen_Area
  schengencountryfile = open(SCHENGEN_COUNTRY_MASTER_DATA, mode = 'r', newline='')
  schengencountryreader = csv.reader(schengencountryfile,delimiter=',') 
  fileList.append( schengencountryfile )
  for schengencountryrow in schengencountryreader:
      schengen_country[ schengencountryrow[0].lower() ] = True
  
  #source : https://en.wikipedia.org/wiki/Eurozone
  eurozonecountryfile = open(EUROZONE_COUNTRY_MASTER_DATA, mode = 'r', newline='')
  eurozonecountryreader = csv.reader(eurozonecountryfile,delimiter=',') 
  fileList.append( eurozonecountryfile )
  for eurozonecountryrow in eurozonecountryreader:
      eurozone_country[ eurozonecountryrow[0].lower() ] = True

  #source : https://en.wikipedia.org/wiki/Group_of_Seven
  g7countryfile = open(G7_COUNTRY_MASTER_DATA, mode = 'r', newline='')
  g7countryreader = csv.reader(g7countryfile,delimiter=',') 
  fileList.append( g7countryfile )
  for g7countryrow in g7countryreader:
      g7_country[ g7countryrow[0].lower() ] = True
  
  #source : monitored_countries.csv
  monitoredcountryfile = open(MONITORED_COUNTRY_MASTER_DATA, mode = 'r', newline='')
  monitoredcountryreader = csv.reader(monitoredcountryfile,delimiter=',') 
  fileList.append( monitoredcountryfile )
  for monitoredcountryrow in monitoredcountryreader:
      monitored_country[ monitoredcountryrow[0].lower() ] = True
      monitored_countries.append(monitoredcountryrow[0].lower())
  
  #hard-ingesting 1 :
  pop[ 'morocco' ] = 35848931 #source : https://www.hcp.ma
  
  #taux_urbanisation = 63.4
  
  #landarea[ 'morocco' ] =	446550 #source : https://www.populationdata.net
  
  #density[ 'morocco' ] = int(float(pop[ 'morocco' ]) / float(landarea[ 'morocco' ]))
  density[ 'morocco' ] = 79.46 #source : https://www.populationdata.net
  
  pattern = re.compile("[^$]+[,]$")
  corrupted_row = False
  column_labels_Flag = True
  country_as_since_day0 = {}
  date = []
  
  morocco_day_zero=datetime.strptime("2020-03-02", "%Y-%m-%d")
  
  #datetime.now() 
  
  covidspecificfile = open(JHU_DATA_FORMATTED_DATA_SET, mode = 'r', newline='')
  fileList.append( covidspecificfile )
  covidspecificreader = csv.reader(covidspecificfile,delimiter=',')
  
  #PASSE 1 :
  #calcul country_day_zero_case_index & country_days_since_day0 pour le point de référence Maroc nécessaire pour les calculs des distances
  
  for rowpasse1 in covidspecificreader:
     if (rowpasse1 [Province_State].lower() != ""):
        rowpasse1 [Country_Region] = rowpasse1 [Country_Region][0].upper()+rowpasse1 [Country_Region][1:].lower()+"-"+rowpasse1 [Province_State][0].upper()+rowpasse1 [Province_State][1:].lower()

     if (rowpasse1[Country_Region].lower() in monitored_countries):
         country_row[ rowpasse1[Country_Region].lower() ] = rowpasse1
         i = Longitude + 1
         while ( (i < len (rowpasse1)) and (int(rowpasse1[i]) == 0)):
             i+=1
         if (i < len(rowpasse1)):
            country_day_zero_case_index[ rowpasse1[Country_Region].lower() ] = i
            country_days_since_day0 [ rowpasse1[Country_Region].lower() ] = len (rowpasse1) - country_day_zero_case_index[ rowpasse1 [Country_Region].lower() ]
 
  covidspecificfile.close()
  covidspecificfile = open(JHU_DATA_FORMATTED_DATA_SET, mode = 'r', newline='')

  covidspecificreader = csv.reader(covidspecificfile,delimiter=',')  

  for row in covidspecificreader:
         #print(row)
         if column_labels_Flag == True:
            column_labels_Flag = False
            maintenant=datetime.strptime(row[len(row)-1], "%m/%d/%Y")
            morocco_virus_age = (maintenant - morocco_day_zero).days + 1
            print(row[1], end = '')
            j = Longitude +1
            while (j < len (row)):
                #dates columns
                date.append ( datetime.strptime(row[j], "%m/%d/%Y") )
                data = date[j-Longitude-1].strftime("%m/%d/%Y")
                print (",",data, end = '')
                j+=1
            print (",","continent", end = '')
            print (",","climatic_zone_I", end = '')
            print (",","climatic_zone_II", end = '')
            print (",","arab_country", end = '')
            print (",","icesco_country", end = '')
            print (",","schengen_country", end = '')
            print (",","eurozone_country", end = '')
            print (",","G7_country", end = '')
            print (",","monitored_country", end = '')
            print (",","population", end = '')
            print (",","density", end = '')
            print (",","medium_age", end = '')
            #print (",","median_age", end = '')
            print (",","urban_population_rate", end = '')
            print (",","#days_since_day_zero", end = '')
            print (",","rate_by_1M_of_inhabitants", end = '')
            #REMOVED SINCE FUTURE INTEREST
            #print (",","mean_gradient", end = '')
            #print (",","raison_moyenne", end = '')
            #print (",","#cases_at_same_morocco_stade", end = '')
            #print (",","mean_gradient_at_same_morocco_stade", end = '')
            for country in monitored_countries:
                print (",","distance_with_"+country, end='')
                print (",","D0_greater_or_equal_than_"+country, end='')
            print(" ");
         else:
            no_case = False
            only_one_new_case = False
            corrupted_row = False
            
            if (row [Province_State].lower() != ""):
              tempon = row [Country_Region].lower() #je mémorise le pays dans une zone qui ne sera plus utilisée
              row [Country_Region] = row [Country_Region][0].upper()+row [Country_Region][1:].lower()+"-"+row [Province_State][0].upper()+row [Province_State][1:].lower()
              row [Province_State] = tempon
              #print (row [Province_State].lower(),"-",row [Country_Region].lower(), end = '')
            else:
              row [Province_State] = row [Country_Region].lower()#je mémorise le pays dans une zone qui ne sera plus utilisée
            # search for first not null day_case_number
            i = Longitude + 1
            while ( (i < len (row)) and (int(row[i]) == 0)):
             i+=1
            if (i < len(row)):
              country_day_zero_case_index[ row [Country_Region].lower() ] = i
              #stocker current_row_date_zero
              current_row_day_zero = date[ country_day_zero_case_index[ row [Country_Region].lower() ] - Longitude - 1 ]
            else:
              no_case = True
            j = i+1
            sum_derivees = 0
            sum_derivees_for_same_morocco_stage = 0
            cases_at_same_morocco_stage = "NA"
            prod_raisons = 1
            if (j == len (row)):
               only_one_new_case = True
            else:
               while (j < len (row)) :
                  sum_derivees = sum_derivees + (int(row[j]) - int(row[j-1]))
                  #tant que (date(dated_value) - current_row_day_zero +1 <= morocco_virus_age)
                  if (current_row_day_zero <= morocco_day_zero) and (((date[ j-Longitude-1 ] - current_row_day_zero ).days + 1 )<= morocco_virus_age):
                      #	calculer le gradient moyen at same morocco stade
                      sum_derivees_for_same_morocco_stage = sum_derivees_for_same_morocco_stage + (int(row[j]) - int(row[j-1]))
                      #stocker la dernière dated_value at same morocco stade
                      cases_at_same_morocco_stage = row[ j ]
                  if int(row[j-1]) != 0:
                    prod_raisons = prod_raisons * (int(row[j]) / int(row[j-1]))
                  else:
                    corrupted_row = True
                  j+=1
            kept = False
            if (corrupted_row == False) and (no_case == False) and (only_one_new_case == False):
                country_days_since_day0 [row [Country_Region].lower()] = len (row) - country_day_zero_case_index[ row [Country_Region].lower() ] 
                moy_arith_derivees = sum_derivees / (country_days_since_day0 [row [Country_Region].lower()] - 1)
                if (current_row_day_zero <= morocco_day_zero): 
                     moy_arith_derivees_at_same_morocco_stage = sum_derivees_for_same_morocco_stage / (morocco_virus_age - 1)  
                else:
                     moy_arith_derivees_at_same_morocco_stage = "NA"
                moy_geo_raisons = prod_raisons ** (1.0 / (country_days_since_day0 [row [Country_Region].lower()] - 1))
                #concatenation and print of Province_State & Country_Region values
                
                print ( row [Country_Region], end = '')
                #else:
                #print Latitude & Longitude
                #print (",", row [Latitude],",",row [Longitude], end = '')
                i = Longitude + 1
                while (i < len (row)):
                    if (i == D18932020_key):  
                         #hard-ingesting 2
                         if (row[Country_Region].lower() == 'morocco') and (specific=='confirmed'): 
                               print (",", D18932020_value, end = '')
                         else:
                               print (",", row [i], end = '')
                    else:
                          print (",", row [i], end = '')
                    i+=1
                kept = True
            elif (corrupted_row == True) or (only_one_new_case == True):
                country_days_since_day0 [row [Country_Region].lower()] = len (row) - country_day_zero_case_index[ row [Country_Region].lower() ] 
    
                if (only_one_new_case == False):
                   moy_arith_derivees = sum_derivees / (country_days_since_day0 [row [Country_Region].lower()] - 1)
                else:
                   moy_arith_derivees = "NA"
                if (only_one_new_case == False) and (current_row_day_zero <= morocco_day_zero): 
                    moy_arith_derivees_at_same_morocco_stage = sum_derivees_for_same_morocco_stage / (morocco_virus_age - 1)  
                else:
                    moy_arith_derivees_at_same_morocco_stage = "NA"
                moy_geo_raisons = "NA"
                #concatenation and print of Province_State & Country_Region values
                print (row [Country_Region][0].upper()+row [Country_Region][1:], end = '')
                # print Latitude & Longitude
                #print (",", row [Latitude],",",row [Longitude], end = '')
                i = Longitude + 1
                while (i < len (row)): 
                   print (",", row [i], end = '')
                   i+=1
                kept = True
            if (kept == True):
              if row [Province_State].lower() in continent.keys(): #row [Province_State] stocke row[Country_Region] sans "-" (seul endroit de cet usage
                 print(",", continent[row [Province_State].lower()], end = '')
              else:
                 print(",NA", end = '')
              #http://www.polaris.iastate.edu/NorthStar/Unit5/unit5_sub1.htm
              #https://en.wikipedia.org/wiki/Geographical_zone
              #http://www.polaris.iastate.edu/NorthStar/Unit5/unit5_sub1.htm
              #https://en.wikipedia.org/wiki/Temperate_climate
              #https://www.wikiwand.com/en/Subarctic
              #https://www.earthonlinemedia.com/ebooks/tpe_3e/essentials/geographical_zones.html
              #One of the most important factors determining your climate is your latitude.
              
              #https://www.earthonlinemedia.com/ebooks/tpe_3e/essentials/geographical_zones.html
              #North Polar: [ +75 (N) , +90 (N)]
              #Arctic: [ +60 (N) , +75 (N) [
              #Subarctic: [ +55 (N) , +60 (N) [
              #Midlatitude: [ 35 (N) , 55 (N) [
              #Subtropical: [ 25 (N) , 35 (N) [
              #Tropical:  [ 10 (N) , 25 (N) [
              #Equatorial: [ -10 (S) , +10 (N) [
              #Tropical:  [ -25 (S) , -10 (S) [
              #Subtropical: [ -35 (S) , -25 (S) [                            
              #Midlatitude: [ -55 (S) , -35 (S) [ 
              #Subantarctic: [ -60 (S) , -55 (S) [
              #Antarctic: [ -75 (S) , -60 (S) [
              #South Polar: [ -90 (S) , -75 (S) [
              if ( float(row [Latitude]) >= +75 ):
                 print(",", "Zone 1 - North Polar", end = '')
              elif ( float(row [Latitude]) >= +60 ) and ( float(row [Latitude]) < +75 ):
                 print(",", "Zone 2 - Arctic", end = '')
              elif ( float(row [Latitude]) >= +55 ) and ( float(row [Latitude]) < +60 ):
                 print(",", "Zone 3 - Subarctic", end = '')
              elif ( float(row [Latitude]) >= +35 ) and ( float(row [Latitude]) < +55 ):
                 print(",", "Zone 4 - Midlaltitude - North", end = '')
              elif ( float(row [Latitude]) >= +25 ) and ( float(row [Latitude]) < +35 ):
                 print(",", "Zone 5 - Subtropical - North", end = '')
              elif ( float(row [Latitude]) >= +10 ) and ( float(row [Latitude]) < +25 ):
                 print(",", "Zone 6 - Tropical - North", end = '')
              elif ( float(row [Latitude]) >= -10 ) and ( float(row [Latitude]) < +10 ):
                 print(",", "Zone 7 - Equatorial", end = '')
              elif ( float(row [Latitude]) >= -25 ) and ( float(row [Latitude]) < -10 ):
                 print(",", "Zone 6 - Tropical - South", end = '')
              elif ( float(row [Latitude]) >= -35 ) and ( float(row [Latitude]) < -25 ):
                 print(",", "Zone 5 - Subtropical - South", end = '')
              elif ( float(row [Latitude]) >= -55 ) and ( float(row [Latitude]) < -35 ):
                 print(",", "Zone 4 - Midlaltitude - South", end = '')   
              elif ( float(row [Latitude]) >= -60 ) and ( float(row [Latitude]) < -55 ):
                 print(",", "Zone 3 - Subantarctic", end = '')
              elif ( float(row [Latitude]) >= -75 ) and ( float(row [Latitude]) < -60 ):
                 print(",", "Zone 2 - Antarctic", end = '')
              elif ( float(row [Latitude]) < -75 ):
                 print(",", "Zone 1 - South Polar", end = '') 
              
              #https://en.wikipedia.org/wiki/Geographical_zone
              #North frigid zone : [ +66.5 (N) Arctic Circle , +90 (N) North Pole ]. Covers 4.12% of Earth's surface.
              #North temperate zone : [ +23.5 (N) Tropic of Cancer , +66.5 (N) Arctic Circle [. 25.99% of Earth's surface.
              #Torrid zone : [ -23.5 (S) Tropic of Capricorn , +23.5 (N) Tropic of Cancer [. Covers 39.78% of Earth's surface.
              #South temperate zone : [ -66.5 (S) Antarctic Circle , -23.5 (S) Tropic of Capricorn [. Covers 25.99% of Earth's surface.
              #South frigid zone : [ -90 (S) South Pole , -66.5 (S) Antarctic Circle [. Covers 4.12% of Earth's surface.
              if ( float(row [Latitude]) >= +66.5 ):
                 print(",", "Zone 1 - Frigid zone - North", end = '')
              elif ( float(row [Latitude]) >= 23.5 ) and ( float(row [Latitude]) < +66.5 ):
                 print(",", "Zone 2 - Temperate Zone - North", end = '')
              elif ( float(row [Latitude]) >= -23.5 ) and ( float(row [Latitude]) < +23.5 ):
                 print(",", "Zone 3 - Torrid zone", end = '')
              elif ( float(row [Latitude]) >= -66.5 ) and ( float(row [Latitude]) < -23.5 ):
                 print(",", "Zone 2 - Temperate Zone - South", end = '')
              elif ( float(row [Latitude]) < -66.5 ):
                 print(",", "Zone 1 - Frigid zone - South", end = '') 
              if row[Country_Region].lower() in arab_country.keys():
                 print(",", "True", end = '')
              else:
                 print(",", "", end = '')
              if row[Country_Region].lower() in icesco_country.keys():
                 print(",", "True", end = '')
              else:
                 print(",", "", end = '')
              if row[Country_Region].lower() in schengen_country.keys():
                 print(",", "True", end = '')
              else:
                 print(",", "", end = '')
              if row[Country_Region].lower() in eurozone_country.keys():
                 print(",", "True", end = '')
              else:
                 print(",", "", end = '')
              if row[Country_Region].lower() in g7_country.keys():
                 print(",", "True", end = '')
              else:
                 print(",", "", end = '')
              if row[Country_Region].lower() in monitored_country.keys():
                 print(",", "True", end = '')
              else:
                 print(",", "", end = '')
              if row[Country_Region].lower() in pop.keys():
                 print(",", pop[row [Country_Region].lower()], ",", density[row [Country_Region].lower()], ",", mediumage[row [Country_Region].lower()], ",", urban_population_rate[row [Country_Region].lower()],end = '')
              else:
                 print(",NA,NA,NA,NA", end = '')
              print(",", country_days_since_day0 [row [Country_Region].lower()], end = '')
              if ( row [Country_Region].lower() in pop.keys() ):
                      print(",", (int(row [len(row)-1]) * 1000000) / int(pop[row [Country_Region].lower()]), end = '')
              else:
                      print(",NA", end ='')
              #REMOVED SINCE FUTURE INTEREST
              #, ",", moy_arith_derivees,",", moy_geo_raisons, ",", cases_at_same_morocco_stage , ",", moy_arith_derivees_at_same_morocco_stage, end = '')
              #cluster_distance = [150, 250, 350, 450] #Je pense relatif à la position du pays scaling !!!!
              for country in monitored_countries:
                 compute_distance(row, country_row[ country ])
                 print(",", distance[ ( row [Country_Region].lower(), country)], end = '')
                 if (country_days_since_day0 [row [Country_Region].lower()] >= country_days_since_day0[country]):
                   #   sphere = 0
                   #   while (sphere < len(cluster_distance)) and (distance[ ( row [Country_Region].lower(), country)] > cluster_distance[sphere]) :
                   #      sphere += 1
                   #   if (sphere < len(cluster_distance)):
                   #      cluster[ ( country, cluster_distance.index(cluster_distance[sphere])+1 ) ].append( row [Country_Region].lower() )
                   #       print("\n=================>",row [Country_Region].lower(),"IN CLUSTER ",cluster_distance[sphere], " [",country,cluster_distance.index(cluster_distance[sphere])+1," ] with DIST=",distance[ ( row [Country_Region].lower(), country)])
                     print(",1", end = '')
                 else:
                     print(",0", end = '')
              #for country in monitored_countries:
              #  for sphere in range(1,5): #{1,4}
              #     if (row [Country_Region].lower() in cluster[ ( country, sphere ) ]):
              #          print(",1", end = '')
              #     else:
              #          print(",0", end = '')
              print(" ")
  closeOpenFiles()

if __name__ == '__main__':
    main()
