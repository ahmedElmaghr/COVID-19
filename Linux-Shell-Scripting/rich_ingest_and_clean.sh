#!/bin/sh

#Author : Karim Baïna, ENSIAS, Mohammed V University
#Research Team : Alqualsadi research team (Innovation on Digital and Enterprise Architectures)
#                ADMIR Laboratory, Rabat IT Center,
#Institution : ENSIAS, University Mohammed V in Rabat,
#              BP 713 Agdal, Rabat, Morocco
#e-mail : karim.baina@um5.ac.ma, karim.baina@gmail.com
#twitter : @kbaina
#Date : April 4nd, 2020
#http://ensias.um5.ac.ma/professor/m-karim-baina
#https://scholar.google.com/citations?user=tRtXdkEAAAAJ&hl=fr
#http://www.slideshare.net/kbaina
#https://gitlab.com/kbaina

############################################################################ INTRODUCTION ############################################################################################
#
# Script Goal : 
# Script Name : rich_ingest_and_clean.sh
# Script Goal : The current script is a data preparation Linux/Unix Shell script that formats Johns Hopkins University 
# COVID19 three files ./COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv, 
#                     ./COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv, 
#                 and ./COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv
# to be ready for data analytics according to four different needs :
#
# Output 1 : removing or replacing specific characters, and formatting dates columns names necessary for dates arithmetics and for general use,
# Output 1 name : time_series_covid19_${specific}_global-tmp.csv
#
# Output 2 : merging two first key columns, and removing null values with keeping column names first line,
# Output 2 name : time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv
#
# Output 3 : merging two first key columns, removing null values, and removing column names first line,
# Output 3 name : time_series_covid19_${specific}_global-sparse.csv
#
# Output 4 : shifting all countries COVID-19 curves to D0 (Date of first not-null Value either for confirmed, deaths or recovered)
#            necessary for comparing evolution shapes independently of first date for confirmed cases, deaths, or recovered cases
# Output 4 name : time_series_covid19_${specific}_global-sparse-shifted-to-D0.csv
#
######################################################################################################################################################################################

############################################################################ INPUT ###################################################################################################
#
# Script Input ($1) : one value among 'confirmed' or 'deaths' or 'recovered'
# value used as parameter for naming pulled gihub Johns-Hopkins University data source file (FILE) : ./COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_${specific}_global.csv
#
# example of script call :
# chmod u+x ./rich_ingest_and_clean.sh
# ./rich_ingest_and_clean.sh confirmed
# ./rich_ingest_and_clean.sh deaths
# ./rich_ingest_and_clean.sh recovered
#
######################################################################################################################################################################################

specific=$1


############################################################################ OUTPUT 1 ################################################################################################
#
# Generate :
# Output 1 : a file that removes \”, ’∗’ characters, replaces non separator ’,’ by ’-’ character (e.g. in "Korea, South"), 
#            formats column dates into "%m/%d/%Y" format (eg. 3/2/20 becomes 03/02/2020)
#            date formatted columns enable any further date arithmetics and manipulation (e.g. duration calculations, D0 of COVID-19, D0 of n^th death manipulation, etc.)
# Output 1 name : time_series_covid19_${specific}_global-tmp.csv
#
#######################################################################################################################################################################################

INPUT_JHU_FILE=./COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_${specific}_global.csv

sed "s/, /-/" ${INPUT_JHU_FILE} |
sed "s/\"//g" |
sed "s/\*//" | sed -E "s/\,(.)\//,0\1\//g" |
sed -E "s/\/(.)\//\/0\1\//g" |
sed -E "s/\/20([^/])/\/2020\1/g" |
sed -E "s/\/20$/\/2020/g"| sed -E "s/,($)/,0\1/g"  > time_series_covid19_${specific}_global-tmp.csv


############################################################################ OUTPUT 2 #################################################################################################
#
# Generate :
# Output 2 : a sparse file with columns (e.g. for parse Matrix NoSQL storage)
#            keeps only not null values from the sparse matrix, and merges the two first columns to form a composite key separated by a ’~’ character.
# Output 2 name : time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv
#
#######################################################################################################################################################################################

sed "s/,0,/,,/g" time_series_covid19_${specific}_global-tmp.csv |
sed  -E "s/([,]+)0,/\1,/g" |
sed -E "s/,0($)/,\1/" |
sed  "s/^,/~/" |
sed -E "s/([a-z A-Z]+),([a-z A-Z]+)/\1~\2/" > time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv


#############################################################################  OUTPUT 3 ###############################################################################################
#
# Generate :
# Output 3 : a sparse file without columns lines (e.g. for parse Matrix NoSQL storage)
# Output 3 name : time_series_covid19_${specific}_global-sparse.csv
#                          which is Output2 (time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv) but without columns names line
#
#######################################################################################################################################################################################

tail -n +2 time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv > time_series_covid19_${specific}_global-sparse.csv


#############################################################################  OUTPUT 4 ###############################################################################################
#
# Generate :
# Output 4 : sparse file with evolution row values (curves) shifted left to D0 (Date of first not-null Value either for confirmed, deaths or recovered)
# Output 4 name : time_series_covid19_${specific}_global-sparse-shifted-to-D0.csv
#
#######################################################################################################################################################################################

./days.sh `head -n 1 time_series_covid19_${specific}_global-sparse-with-formatted-column-names.csv |
sed "s/,/\t/g"` > time_series_covid19_${specific}_global-sparse-shifted-to-D0.csv

cat time_series_covid19_${specific}_global-sparse.csv |
sed -E "s/([,]+)((,[0-9]+)((,[0-9]*)*))/\2\1/" |
sed "s/^~//" |
sed "s/~/-/" >> time_series_covid19_${specific}_global-sparse-shifted-to-D0.csv


#sed -E "s/([,]+)((,[0-9]+)+))/\2\1/" would more simple and more logical than sed -E "s/([,]+)((,[0-9]+)((,[0-9]*)*))/\2\1/" however some dirty data contains null values  (i.e. ,, sparse coding) between two not null values.

